-------------------------------------------------------------------------------
-- Purpose:
-- Registers the default action which can be modified with params
--
-- Behavior: stand still and think for a bit. aka do nothing
--
-- This is considered the default action which other actions are based on.
-- Changing this can change other actions so leave it as doing nothing.
-- This sets the action manually, but normally it would use register_action
-- instead
-------------------------------------------------------------------------------

local null_func = emergent_ai.null_function
local name = "idle"
emergent_ai.registered_actions[name] = {
  name = name, -- MUST be the same as the index
  on_step = null_func,
  on_action_complete = null_func,
  on_action_start = null_func,
  on_task_complete = null_func,
  preferred_actions = {
    idle = 2, -- 2x as likely
    null = 0, -- don't do
  }
}