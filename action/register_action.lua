-------------------------------------------------------------------------------
-- Purpose:
-- Provide method for creating a new action type and adding it to the list
-------------------------------------------------------------------------------

emergent_ai.register_action = function(params)
  local action = params
  action = emergent_ai.deep_copy_conditional(emergent_ai.registered_actions.idle, action, true)
  emergent_ai.registered_actions[params.name] = action
end