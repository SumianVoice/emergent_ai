-------------------------------------------------------------------------------
-- Purpose:
-- Call the other files in the folder
-------------------------------------------------------------------------------
local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)
local S = minetest.get_translator(mod_name)

-- must do idle first, since all others use it as a base
dofile(mod_path .. DIR_DELIM .. "action" .. DIR_DELIM .. "idle.lua")
dofile(mod_path .. DIR_DELIM .. "action" .. DIR_DELIM .. "register_action.lua")
dofile(mod_path .. DIR_DELIM .. "action" .. DIR_DELIM .. "build.lua")
dofile(mod_path .. DIR_DELIM .. "action" .. DIR_DELIM .. "dig.lua")
dofile(mod_path .. DIR_DELIM .. "action" .. DIR_DELIM .. "fight.lua")
dofile(mod_path .. DIR_DELIM .. "action" .. DIR_DELIM .. "roam.lua")