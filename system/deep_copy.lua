-------------------------------------------------------------------------------
-- Purpose:
-- Provide a recursive, conditional deep copy function
-------------------------------------------------------------------------------

-- Fills in holes in the data with some other data.
-- Prevents "attempt to index nil".
-- e.g. if there was an object {a=1,b=2} and another one which didn't have
-- all the same data, like {a=1} then it would crash when trying to access something.b(param)
-- Also helps with applying defaults
-- This is slow, don't use it realtime, only on startup
emergent_ai.deep_copy_recursive = function(copyfrom, copyto, conditional)
  for index, value in pairs(copyfrom) do
    local copyto_current = copyto[index]
    -- if it's another table then go through that table and copy that
    if type(value) == "table" then
      if copyto_current == nil then copyto_current = {} end -- if it doesn't have the key then make one
      copyto_current[index] = emergent_ai.deep_copy_recursive(value, copyto_current, conditional)
    -- otherwise only add data if it's not yet present
    elseif copyto_current[index] == nil or not conditional then
      copyto_current[index] = value
    end
  end
end

-- copies all the data from [copyfrom] to [copyto] but only if the data in
-- [copyto] is not set yet. AKA don't override [copyto] data
emergent_ai.deep_copy_conditional = function(copyfrom, copyto, conditional)
  copyto = emergent_ai.deep_copy_recursive(copyfrom, copyto, conditional)
  return copyto
end