-------------------------------------------------------------------------------
-- Purpose:
-- Call the other files in the folder
-------------------------------------------------------------------------------
local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)
local S = minetest.get_translator(mod_name)

dofile(mod_path .. DIR_DELIM .. "data" .. DIR_DELIM .. "node_list.lua")