# Emergent_AI

Tries to add emergent AI to minetest (and probably fails)

## Status: not yet functional
This project is huge, and currently non-functioning. It needs more development
 before you can actually see anything in game or even run it without errors.

## Goals
* some kind of AI that can interact with the world in emergent ways
* an API to allow for this to be implemented into other mods
* API first, implementation second
* be 100% standalone
* be extensible, since it will be huge to add all this at once


## Contributing
* don't bloat files, add new files if you need to add a new capability
* the single purpose of a file should be declared at the start of the file in a comment
* using the word "and" in the above description is not allowed
* init.lua should only handle `dofile` and `emergent_ai = {}`
* each file should have one large scale purpose such as "decide what the mob will do next"
* all methods should be exposed in the root global variable `emergent_ai`
* there should be no way to break the mod just by running a different game, don't use groups or explicit item names unless they are culled if and when they aren't available: no "attempt to index nil" under any circumstances
* for releases, there should be no effect on the game, no items added unless a setting is set; this adds the emergent AI but shouldn't force the player to use mobs that use this API


## Intended Scope
* AI can dig tunnels, even if it's done in a strange way


## Structure
### Action
Actions are individual tasks that the mob can do, and their associated functions.
 An action can be "roam" or "dig" for example.

### Behaviour
A behaviour is a set of tendencies and a tracking system for these tendencies.
 These can hold actions and have rules about which actions can be done by this
 specific mob. The mob definition uses this. It is made with emergent_ai.new()

### Mob
An entity that has a behaviour and chooses actions based on those behaviours.


## How it will work
1. get a reference to an object containing the behaviour you want, either by copying one or
making a new behaviour with
```LUA
my_mod.my_behaviour = emergent_ai.new(behaviour_information)
```
2. use that object ref to pass functions to your actual mob definition
```LUA
on_step = my_mod.my_behaviour.on_step
```
3. now, when your mob updates, it goes into the emergent_ai system to get behaviours.
4. this assumes that self.object:get_pos and so on are valid methods, so this obviously
cannot work for anything other than normal entities / mobs

## Getting animations
Available animation names by default:
  - idle
  - walk
  - charge
  - interact
  - attack
```LUA
-- in mob definition
animations = {
  idle = {x=0, y=40},
  charge = {x=41, y=80},
}
```