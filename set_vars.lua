-------------------------------------------------------------------------------
-- Purpose:
-- Set variables so that they can be safely modified later
-------------------------------------------------------------------------------

emergent_ai.registered_states = {} -- states like walk to pos
emergent_ai.registered_actions = {} -- stores all actions
emergent_ai.default_actions = {} -- only default / global actions
emergent_ai.registered_behaviors = {} -- stores references to mobs that use emergent_ai.new()
emergent_ai.data = {}
emergent_ai.data.available_nodes = {}
emergent_ai.permissions = {}
emergent_ai.debug = {}