-------------------------------------------------------------------------------
-- Purpose:
-- Provide methods for the emergent_ai object type
-------------------------------------------------------------------------------

-- null function to prevent crashes without having to test for them every time
emergent_ai.null_function = function(param) return param end

-- this is a default emergent_ai object, which defines the behaviours for a certain mob
emergent_ai.defaults = {
  action = "idle", -- the name of the current action being done
  last_action = "idle",
  preferred_actions = {
    idle = 5, -- 5 x more likely than normal
    roam = 2, -- 2 x more likely than normal
  },
  action_permissions = {
    only_allow = false, -- if true, don't allow anything not listed here
    allowed = {
      idle = true,
      null = false,
    },
  },
  action_change_time = 5, -- how long to wait to change action
  action_change_time_random = 20, -- adds random() * this to the normal time
  -- These functions are called at the END of the rest of the calls. Use these
  -- to get information about what's changed
  on_step             = emergent_ai.null_function,
  on_action_complete  = emergent_ai.null_function,
  on_action_start     = emergent_ai.null_function,
  on_task_complete    = emergent_ai.null_function,
}

emergent_ai.defaults.on_task_complete = 9
-- Returns a new emergent_ai object which contains information about the
-- behaviour of the mob
-- The mob definition will be given a reference to this and callbacks to its own functions can be included
local deep_copy = emergent_ai.deep_copy_conditional

emergent_ai.new = function(params)
  local input = deep_copy(params, {}, false)
  local new_definition = deep_copy(emergent_ai.defaults, params, true)
  return new_definition
end
