local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)
local S = minetest.get_translator(mod_name)

emergent_ai = {}
dofile(mod_path .. DIR_DELIM .. "set_vars.lua")

dofile(mod_path .. DIR_DELIM .. "permissions" .. DIR_DELIM .. "permissions.lua")
dofile(mod_path .. DIR_DELIM .. "system" .. DIR_DELIM .. "system.lua")
dofile(mod_path .. DIR_DELIM .. "data" .. DIR_DELIM .. "data.lua")
dofile(mod_path .. DIR_DELIM .. "logic" .. DIR_DELIM .. "logic.lua")
dofile(mod_path .. DIR_DELIM .. "action" .. DIR_DELIM .. "action.lua")
dofile(mod_path .. DIR_DELIM .. "state" .. DIR_DELIM .. "state.lua")
dofile(mod_path .. DIR_DELIM .. "api" .. DIR_DELIM .. "api.lua")