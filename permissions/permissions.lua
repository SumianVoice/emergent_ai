-------------------------------------------------------------------------------
-- Purpose:
-- Provide is blocked check list
-------------------------------------------------------------------------------

-- emergent_ai.permissions
-- disallows a certain action from being performed on the server
emergent_ai.permissions.blocked_actions = {
  null = true, -- true = blocked
}

emergent_ai.permissions.is_blocked = function(table, index_name)
  for index, val in pairs(table) do
    if index == index_name and val then
      return true
    end
  end
  return false
end