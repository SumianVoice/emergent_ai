# emergent_ai API



```LUA
emergent_ai.new(params)
```
Gives an emergent ref.

```LUA
emergent_ai.registered_actions.(action name)
emergent_ai.registered_actions[(action name)]
```

```LUA
emergent_ai.default_actions.(action name)
emergent_ai.default_actions[(action name)]
```

```LUA
emergent_ai.registered_behaviors
```

```LUA
emergent_ai.data.available_nodes
```
Lists all the nodes the mobs can use. By default, it will add any nodes it sees
to this list provided they are:
```LUA
walkable == true
and any of:
  "building_block"
  "pickaxey"
  "shovely"
  "handy"
  "axey"
  "hoey"
  "oddly_breakable_by_hand"
  "choppy"
  "cracky"
  "crumbly"
```
